/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 17:05:08 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 17:58:21 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_check(const char *str, int str_base, int i)
{
	int		result;
	int		b;

	result = 0;
	b = 0;
	while ((str[i] >= 48 && str[i] <= 57) ||
			(str[i] >= 'a' && str[i] < 'a' + (str_base - 10)) ||
			(str[i] >= 'A' && str[i] < 'A' + (str_base - 10)))
	{
		if (str[i] >= 'a' && str[i] <= 'f')
			b = str[i] - 87;
		else if (str[i] >= 'A' && str[i] <= 'F')
			b = str[i] - 55;
		result = result * str_base + b;
		i++;
	}
	return (result);
}

int		ft_atoi_base(const char *str, int str_base)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;
	if (str_base < 2 && str_base > 16)
		return (0);
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
	{
		i++;
		minus = 45;
	}
	if (str_base <= 10)
		while (str[i] >= 48 && str[i] <= 48 + str_base)
			result = result * str_base + str[i++] - '0';
	else
		result = ft_check(str, str_base, i);
	return (result);
}
