/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hidenp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 12:54:47 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 13:23:02 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	int		counter;

	j = 0;
	i = 0;
	counter = 0;
	if (argc != 3)
		return (error());
	while (argv[1][i])
	{
		while (argv[2][j])
		{
			if (argv[1][i] == argv[2][j])
			{
				counter++;
				break ;
			}
			j++;
		}
		i++;
	}
	if ((!argv[1][i] && argv[2][j]) || (!argv[1][i] && counter == i))
		write(1, "1", 1);
	else
		write(1, "0", 1);
	write(1, "\n", 1);
	return (0);
}
