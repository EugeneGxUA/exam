/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_prime_sum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 10:35:46 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 11:21:33 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_atoi(char *str)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
	{
		write(1, "0\n", 2);
		return (0);
	}
	while (str[i] >= 48 && str[i] <= 57)
		result = result * 10 + str[i++] - '0';
	return (result);
}

void	ft_putnbr(int nb)
{
	if (nb >= 10)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else
		ft_putchar(nb + '0');
}

int		ft_check(int i)
{
	int		a;

	a = 2;
	while (a <= i)
	{
		if (i % a == 0)
			break ;
		a++;
	}
	if (i == a)
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		tmp;
	int		result;

	result = 0;
	tmp = 2;
	if (argc != 2)
	{
		write(1, "0\n", 2);
		return (0);
	}
	i = ft_atoi(argv[1]);
	while (i > 1)
	{
		if (ft_check(i))
			result += i;
		i--;
	}
	ft_putnbr(result);
	write(1, "\n", 1);
	return (0);
}
