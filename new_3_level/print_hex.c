/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 16:10:54 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 16:46:39 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_atoi(char *str)
{
	int		i;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
		return (0);
	while (str[i] >= 48 && str[i] <= 57)
		result = result * 10 + str[i++] - '0';
	return (result);
}

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_rec(int i)
{
	int		j;
	int		a;

	a = i / 16;
	j = i - (a * 16);
	if (a != 1 && a != 0)
		ft_rec(a);
	else if (a != 0)
		ft_putchar(a + '0');
	if (j > 9 && j <= 15)
		ft_putchar('a' + (j - 10));
	else if (j >= 0 && j < 10)
		ft_putchar(j + '0');
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	char	a;

	i = 0;
	j = 0;
	if (argc != 2)
		return (error());
	i = ft_atoi(argv[1]);
	ft_rec(i);
	write(1, "\n", 1);
	return (0);
}
