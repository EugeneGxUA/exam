/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrange.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 12:34:39 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 12:53:41 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_rrange(int start, int end)
{
	int		*tab;
	int		len;
	int		i;
	int		a;

	i = 0;
	if (start > end)
		len = start - end;
	else
		len = end - start;
	tab = (int*)malloc(sizeof(int) * len + 1);
	a = start;
	tab[i++] = start;
	while (i <= len)
	{
		if (start > end)
			tab[i] = a -= 1;
		else
			tab[i] = a += 1;
		i++;
	}
	return (tab);
}
