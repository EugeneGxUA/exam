/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pgcd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 15:52:56 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 16:09:02 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		error(void)
{
	printf("\n");
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	int		a;

	i = 0;
	j = 0;
	if (argc != 3)
		return (error());
	i = atoi(argv[1]);
	j = atoi(argv[2]);
	if (i > j)
		a = j;
	else
		a = i;
	while (a >= 1)
	{
		if (j % a == 0 && i % a == 0)
			break ;
		a--;
	}
	printf("%d\n", a);
	return (0);
}
