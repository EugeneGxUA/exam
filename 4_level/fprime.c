/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprime.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 18:02:30 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 18:49:43 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

void	ft_check(int i)
{
	int		a;

	a = 2;
	while (a <= i)
	{
		if (i % a == 0)
		{
			printf("%d", a);
			if (i == a)
				return ;
			printf("*");
			i = i / a;
			a = 1;
		}
		a++;
	}
}

int		main(int argc, char **argv)
{
	int		i;

	i = 0;
	if (argc != 2)
		return (printf("\n"));
	i = atoi(argv[1]);
	if (i == 1)
		printf("1");
	else
		ft_check(i);
	printf("\n");
	return (0);
}
