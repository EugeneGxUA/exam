/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 18:51:49 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 19:25:23 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_len(char *str)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	while (str[i])
	{
		if (str[i] < 33 && str[i - 1] > 33 && str[i] != '\0')
			len++;
		i++;
	}
	return (len);
}

int		ft_words(char *str, int i)
{
	int		j;

	j = 0;
	while (str[i] > 33 && str[i] != '\0')
	{
		i++;
		j++;
	}
	return (j);
}

char	**ft_split(char *str)
{
	int		i;
	int		j;
	char	**s;
	int		a;

	i = 0;
	j = 0;
	s = (char**)malloc(sizeof(char*) * ft_len(str) + 1);
	if (!s)
		return (NULL);
	while (str[i])
	{
		a = 0;
		while (str[i] < 33 && str[i] != '\0')
			i++;
		if (str[i] != '\0')
		{
			s[j] = (char*)malloc(sizeof(char) * ft_words(str, i) + 1);
			while (str[i] > 33 && str[i] != '\0')
				s[j][a++] = str[i++];
			s[j++][a] = '\0';
		}
	}
	s[j] = NULL;
	return (s);
}

int		main()
{
	char **tab;
	char *str = " hello friend yo ";
	int		a;
	a = 0;
	tab = ft_split(str);
	while (tab[a])
		printf("%s\n", tab[a++]);
}
