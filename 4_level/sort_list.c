/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 20:14:58 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 20:27:06 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_list    *sort_list(t_list* lst, int (*cmp)(int, int))
{
	t_list	*tmp;
	t_list	*new;
	int		buf;

	new = lst;
	while (lst->next)
	{
		tmp = lst->next;
		while (tmp)
		{
			if (cmp(lst->data, tmp->data) > 0)
			{
				buf = lst->data;
				lst->data = tmp->data;
				tmp->data = buf;
			}
			tmp = tmp->next;
		}
		lst = lst->next;
	}
	return (new);
}
