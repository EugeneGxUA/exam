/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_wstr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 19:26:21 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/20 19:59:14 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int		ft_word(char *str)
{
	int		j;

	j = 0;
	while (str[j])
		j++;
	while (str[j] < 33)
		j--;
	while (str[j] > 33)
		j--;
	j++;
	return (j);
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	int		a;

	i = 0;
	//j = ft_word(argv[1]);
	if (argc == 2 && j >= 0)
	{
		j = ft_word(argv[1]);
		a = j;
		while (argv[1][i] < 33)
			i++;
		while (argv[1][j] > 33)
			write(1, &argv[1][j++], 1);
		write(1, " ", 1);
		while (i < a - 1)
			write(1, &argv[1][i++], 1);
	}
	write(1, "\n", 1);
	return (0);
}
