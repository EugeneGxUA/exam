/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 20:40:09 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/21 14:29:01 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int		ft_len(char *str)
{
	int		i;
	int		len;

	i = 0;
	len = 0;
	while (str[i])
	{
		if (str[i] > 32 && (str[i + 1] < 33 || str[i + 1]))
			len++;
		i++;
	}
	return (len);
}

int		ft_words(char *str, int i)
{
	int		a;

	a = 0;
	while (str[i] > 33 && str[i])
	{
		i++;
		a++;
	}
	return (a);
}

int		ft_print(char **s)
{
	int		i;
	int		j;

	i = 0;
	j = 1;
	while (s[j])
	{
		while (s[j][i])
			write(1, &s[j][i++], 1);
		i = 0;
		j++;
		write(1, " ", 1);
	}
	int		a;
	a = 0;
	while (s[0][a])
	{
		write(1, &s[0][a], 1);
		a++;
	}
	return (0);
}

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	int		a;
	char	**s;

	i = 0;
	j = 0;
	if (argc != 2)
		return (error());
	s = (char**)malloc(sizeof(char*) * ft_len(argv[1]) + 1);
	if (!s)
		return (0);
	while (argv[1][i])
	{
		a = 0;
		while (argv[1][i] < 33 && argv[1][i])
			i++;
		s[j] = (char*)malloc(sizeof(char) * ft_words(argv[1], i) + 1);
		if (argv[1][i] != '\0')
		{
			while (argv[1][i] > 33 && argv[1][i])
				s[j][a++] = argv[1][i++];
			s[j][a] = '\0';
			j++;
		}
	}
	s[j] = NULL;
	ft_print(s);
	write(1, "\n", 1);
	return (0);
}
