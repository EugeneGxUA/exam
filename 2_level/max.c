/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 08:34:57 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/31 08:55:06 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_max(int* tab, unsigned int len)
{
	int		i;
	int		max;

	i = 0;
	max = 0;
	while (i < len)
	{
		if (max < tab[i])
			max = tab[i];
		i++;
	}
	return (max);
}

int		main()
{
	unsigned int		len;
	int		tab[5] = { 1, 9, 5, 3, 7 };
	int		max;

	len = 5;
	max = ft_max(tab, len);
	printf("%d", max);
	return (0);
}
