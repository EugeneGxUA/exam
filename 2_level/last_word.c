/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   last_word.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 14:14:03 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/30 14:40:11 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
int		main(int argc, char **argv)
{
	int		i;

	i = 0;
	if (argc == 2)
	{
		while (argv[1][i])
			i++;
		while (argv[1][i] < 33 && argv[1])
			i--;
		if (i < 0)
		{
			write(1, "\n", 1);
			return (0);
		}
		while (argv[1][i - 1] > 33 && argv[1])
			i--;
		while (argv[1][i] > 33 && argv[1])
		{
			write(1, &argv[1][i], 1);
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}
