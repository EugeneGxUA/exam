/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 15:47:38 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/31 09:00:46 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		ft_atoi(const char *str)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 45 || str[i] == 43)
		minus = str[i++];
	while (str[i] >= '0' && str[i] <= '9')
		result = result * 10 + str[i++] - '0';
	if (minus == 45)
		result = -result;
	return (result);
}

int main()
{
	char *str = "     2147483647";

	printf("%d", ft_atoi(str));
	return (0);
}
