/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 16:17:17 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/30 19:28:15 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		check(char *str, char c, int i)
{
	i--;
	while (i >= 0)
	{
		if (str[i] == c)
			return (0);
		i--;
	}
	return (1);
}

int		main(int argc, char **argv)
{
	int		i;
	int		len;

	i = 0;
	while (argv[1][i])
	{
		if (check(argv[1], argv[1][i], i))
			write(1, &argv[1][i], 1);
		i++;
	}
	len = i;
	i = 0;
	while (argv[2][i])
	{
		if (check(argv[2], argv[2][i], i))
			if (check(argv[1], argv[2][i], len - 1))
				write(1, &argv[2][i], 1);
		i++;
	}
	return (0);
}
