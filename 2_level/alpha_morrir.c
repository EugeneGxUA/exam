/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alpha_morrir.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 08:25:18 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/31 08:34:46 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		main(int argc, char **argv)
{
	int		i;

	i = 0;
	if (argc == 2)
	{
		while (argv[1][i])
		{
			if (argv[1][i] >= 'a' && argv[1][i] <='z')
			{
				argv[1][i] = 109 - argv[1][i];
				argv[1][i] = 110 + argv[1][i];
			}
			else if (argv[1][i] >= 'A' && argv[1][i] <= 'Z')
			{
				argv[1][i] = 77 - argv[1][i];
				argv[1][i] = 78 + argv[1][i];
			}
			write(1, &argv[1][i], 1);
			i++;
		}
	}
}
