/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 08:55:25 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/31 09:26:14 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}


void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
		write(1, "-2147483648", 11);
	else
	{
		if (nb < 0)
		{
			write(1, "-", 1);
			nb = -nb;
		}
		if (nb >= 10)
		{
//			ft_putnbr(nb / 10);
//			ft_putnbr(nb % 10);
//		}
	
//		else
			ft_putchar(nb % 10 + '0');
			ft_putnbr(nb / 10);
	}
	}
}


int		ft_atoi2(char *str)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 43 || str[i] == 45)
		minus = str[i++];
	while (str[i] >= '0' && str[i] <= '9')
		result = result * 10 + str[i++] - '0';
	if (minus == 45)
		(result = -result);
	return (result);
}

int		main(void)
{
	char *str = "-2147";
	int		n;

	n = 2147;
	ft_putnbr(n);
	//printf("%d", ft_atoi2(str));
	return (0);
}
