/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 17:34:51 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/31 19:23:39 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char 	*ft_strrev(char *str)
{
	int		i;
	int		j;
	char	buf;


	j = ft_strlen(str) - 1;
	i = 0;
	while (j > i)
	{
		buf = str[i];
		str[i] = str[j];
		str[j] = buf;
		i++;
		j--;
	}
	return (str);
}

#include <stdio.h>

int		main(int argc, char **argv)
{
	printf("%s\n", ft_strrev(argv[1]));
	return (0);	
}
