/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_word.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 18:17:10 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/28 16:30:10 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		main(int argc, char **argv)
{
	int		i;

	i = 0;
	if (argc != 2)
	{
		write(1, "\n", 1);
		return (0);
	}
	while (argv[1][i] < 33 && argv[i])
		i++;
	while (argv[1][i] > 33 && argv[i])
		{
			write(1, &argv[1][i], 1);
			i++;
		}
	write(1, "\n", 1);
	return (0);
}
