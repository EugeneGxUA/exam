/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat_alpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 17:56:04 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/21 18:28:59 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		main(int argc, char **argv)
{
	int		i;
	int		count_up;
	int		count_low;

	i = 0;
	if (argc != 2)
	{
		write(1, "\n", 1);
		return (0);
	}
	while (argv[1][i])
	{
		if (argv[1][i] >= 'A' && argv[1][i] <= 'Z')
		{
			count_up = argv[1][i] - 64;
			while (count_up > 0)
			{
				write(1, &argv[1][i], 1);
				count_up--;
			}
		}
		else if (argv[1][i] >= 'a' && argv[1][i] <= 'z')
		{
			count_low = argv[1][i] - 96;
			while (count_low > 0)
			{
				write(1, &argv[1][i], 1);
				count_low--;
			}
		}
		i++;
	}
	return (0);
}
