/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_prime_sum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/05 17:53:52 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/07 16:42:49 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

int		ft_atoi(char *str)
{
	int		i;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
		return (error());
	while (str[i] >= '0' && str[i] <='9')
		result = result * 10 + str[i++] - '0';
	return (result);
}

int		ft_check(int i)
{
	int		a;

	a = 2;
	while (a <= i)
	{
		if (i % a == 0)
			break ;
		a++;
	}
	if (i == a)
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		result;

	result = 0;
	if (argc != 2)
		return(error());
	i = ft_atoi(argv[1]);
	while (i > 1)
	{
		if (ft_check(i))	
			result = result + i;
		i--;
	}	
	printf("%d", result);
	return (0);
}
