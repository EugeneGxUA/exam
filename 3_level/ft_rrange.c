/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrange.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 12:02:28 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 12:33:23 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		*ft_rrange(int start, int end)
{
	int		*tab;
	int		len;
	int		i;
	int		a;

	a = start;
	i = 0;
	if (start < end)
		len = end - start;
	else
		len = start - end;
	tab = (int*)malloc(sizeof(int) * len + 1);
	tab[i++] = start;
	while (i <= len)
	{
		if (start < end)
			tab[i] = a += 1;
		else
			tab[i] = a -= 1;
		i++;
	}
	return (tab);
}

int		main(void)
{
	int		start;
	int		end;
	int		*tab;

	start = -15;
	end = -10;
	tab = ft_rrange(start, end);
	int i = 0;
	while (i < 6)
		printf("%d ", tab[i++]);
	return (0);
}
