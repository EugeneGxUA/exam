/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hidenp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 12:52:16 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 13:19:45 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	int		counter;

	i = -1;
	j = 0;
	counter = 0;
	if (argc != 3)
		return (error());
	while (argv[1][++i] != '\0')
		while (argv[2][j])
		{
			if (argv[1][i] == argv[2][j])
			{
				counter++;
				break ;
			}
			j++;
		}
	if ((!argv[1][i] && argv[2][j]) || (!argv[1][i] && counter == i))
		write(1, "1\n", 2);
	else
		write(1, "0\n", 2);
	return (0);
}
