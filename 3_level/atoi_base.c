/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 17:40:39 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 18:56:39 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_atoi_base(const char *str, int str_base)
{
	int		i;
	int		minus;
	int		result;
	int		b;

	i = 0;
	result = 0;
	if (str_base < 2 || str_base > 16)
		return (0);
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
		minus = str[i++];
	if (str_base <= 10)
		while (str[i] >= 48 && str[i] < (48 + str_base))
			result = result * str_base + str[i++] - '0';
	else
		while ((str[i] >= 48 && str[i] <= 57) || ((str[i] >= 97 && str[i] <= 97 + (str[i] - 10)) || (str[i] >= 65 && str[i] <= (65 + (str[i] - 10)))))
		{	
			if (str[i] >= 65 && str[i] <= 65 + (str[i] - 10))
					b = str[i] - 55;
			else
				b = str[i] - '0';
			result = result * str_base + b;
			i++;
			
		}
	if (str_base == 10 && minus == 45)
		return (-result);
	return (result);
}

int		main(void)
{
	char	*str;

	int		a;

	str = "FF";
	a = 16;
	printf("%d", ft_atoi_base(str, a));
	return (0);
}
