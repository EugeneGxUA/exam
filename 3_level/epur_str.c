/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   epur_str.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/05 18:06:35 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/07 17:53:03 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		main(int argc, char **argv)
{
	int		i;

	i = 0;
	if (argc != 2 || argv[1][i] == '\0')
	{
		write(1, "\n", 1);
		return (0);
	}
	while (argv[1][i])
	{
		while (argv[1][i] < 33 && argv[1][i])
			i++;
		while (argv[1][i] > 33 && argv[1][i + 1] != '\0')
		{
			write(1, &argv[1][i], 1);
			i++;
		}
		while (argv[1][i] < 33 && argv[1][i])
			i++;
		if (argv[1][i] != '\0')
			write(1, " ", 1);
	}
	write(1, "\n", 1);
	return (0);
}
