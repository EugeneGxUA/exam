/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 13:25:32 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 16:27:41 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>

int		ft_atoi(char *str)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 45 || str[i] == 43)
		minus = str[i++];
	while (str[i] >= '0' && str[i] <= '9')
		result = result * 10 + str[i++] - '0';
	if (minus == 45)
		return (0);
	return (result);
}

int		main(int argc, char **argv)
{
	int		i;
	int		tmp;
	char	chr;

	i = ft_atoi(argv[1]);
	printf("%d\n", i);
	if (i == 0)
	{
		write(1, "0\n", 2);
		return (0);
	}
	while (i > 0)
	{
		tmp = i % 16;
		if (tmp >= 10)
		{
			tmp -= 10;
			chr = tmp + 'a';
			write(1, &chr, 1);
		}
		else
		{
			chr = tmp + '0';
			write(1, &chr, 1);
		}
		i = i / 16;
	}
	return (0);
}
