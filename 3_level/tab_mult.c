/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_mult.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 17:02:00 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 17:47:17 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_atoi(char *str)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
		minus = str[i];
	while (str[i] >= '0' && str[i] <= '9')
		result = result * 10 + str[i++] - '0';
	if (result == 0)
	{
		write(1, "0\n", 2);
		return (0);
	}
	if (minus == 45)
		return (-result);
	return (result);
}

void	ft_putnbr(int nb)
{
	if (nb >= 10)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else
		ft_putchar(nb + '0');
}

int		main(int argc, char **argv)
{
	int		i;
	int		tmp;
	char	chr;
	int		b;

	chr = '1';
	b = 1;
	if (argc != 2)
		return (error());
	i = ft_atoi(argv[1]);
	if (i < 0)
	while (chr <= '9')
	{
		write(1, &chr, 1);
		write(1, " x ", 3);
		ft_putnbr(i);
		write(1, " = ", 3);
		tmp = b * i;
		ft_putnbr(tmp);
		write(1, "\n", 1);
		chr++;
		b++;
	}
	return (0);
}
