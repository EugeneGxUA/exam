/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 20:45:02 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 21:00:23 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_atoi_base(const char *str, int str_base)
{
	int		i;
	int		minus;
	int		result;

	i = 0;
	result = 0;

	if (str_base < 2 || str_base > 16)
		return (0);
	while (str[i] < 33)
		i++;
	if (str[i] == 45)
		minus = str[i++];
	if (str_base <= 10)
		while (str[i] >= 48 && str[i] <= 48 + str[i])
			result = result * str_base + str[i++] - '0';
	else
		while (str[i] >= '0' && str[i] <= '0' + str[i] || (str[i] >= 'a'
					&& <= 'a' + (str[i] - 10)) || (str[i] >= 'A' && str[i]
					<= 'A' + (str[i] - 10)))
		{	
			if (str[i] >= 65 && str[i] <= 65 + (str[i] - 10))
				b = str[i] - 55;
			else
				b = str[i] - '0';
			result = result * str_base + b;
			i++;
		}
	if (str_base == 10 && minus == 45)
		return (-result);
	return (result)
}
