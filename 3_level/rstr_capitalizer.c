/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstr_capitalizer.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 15:14:09 by egaragul          #+#    #+#             */
/*   Updated: 2017/02/06 16:51:04 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		error(void)
{
	write(1, "\n", 1);
	return (0);
}

void	ft_check(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] += 32;
		if (str[i] >= 'a' && str[i] <= 'z' && (str[i + 1] < 33
					|| str[i + 1] == '\0'))
			str[i] -= 32;
		write(1, &str[i], 1);
		i++;
	}
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;

	i = 0;
	j = 1;
	if (argc == 1)
		return (error());
	while (argv[j])
	{
		ft_check(argv[j]);
		write(1, "\n", 1);
		j++;
	}
	return (0);
}
