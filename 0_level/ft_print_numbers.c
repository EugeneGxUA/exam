/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_numbers.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: egaragul <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 16:19:03 by egaragul          #+#    #+#             */
/*   Updated: 2017/01/21 16:24:55 by egaragul         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_numbers(void)
{
	int		i;
	
	i = '0';
	while ('9' >= i)
	{
		write(1, &i, 1);
		i++;
	}
	return;
}

int main()
{
	ft_print_numbers();
	return (0);
}
